package com.example.ApeEats;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;


public class ResHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    MenuItem addProduct;
    RelativeLayout addBranch;
    TextView btnProductList;

    FirebaseAuth auth;
    ActionBarDrawerToggle mToogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res_home);
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(getApplicationContext(), RestaurantSignin.class));
            finish();
        }

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        addBranch = findViewById(R.id.btnAddBranch);
        btnProductList = findViewById(R.id.btnProductList);


        //To hide or show items
        //Menu menu = navigationView.getMenu();
        //menu.findItem(R.id.nav_addProduct).setVisible(false);

        setSupportActionBar(toolbar);

        navigationView.bringToFront();
        mToogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(mToogle);
        mToogle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        addBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), createAccount.class);
                startActivity(i);
            }
        });

        btnProductList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),ProductList.class);
                startActivity(i);
            }
        });

//        String checkUser = Objects.requireNonNull(auth.getCurrentUser()).getUid();
//
//        Log.d("Hello",checkUser);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_addProduct) {
            Intent intent = new Intent(getApplicationContext(), AddProduct.class);
            startActivity(intent);
        }
        if (item.getItemId() == R.id.nav_logOut) {
            try {
                auth.signOut();
                Intent i = new Intent(getApplicationContext(),RestaurantSignin.class);
                startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

}