package com.example.ApeEats;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ApeEats.models.ProductModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class EditProduct extends AppCompatActivity {

    String getUID;

    ImageView updateImg,rAddimage,rplus;
    EditText updateName;
    Spinner updateCategory;
    EditText updateDesc;
    EditText updatePrice;
    RadioGroup updateRadio;
    RadioButton radio1,radio2;

    String foodname, price, description, yradio, addimgUri, Categ;

    Boolean setState = true; //this must be true if uploaded image is ok.

    Button btnUpdate;

    Uri rimgUri;

    DatabaseReference db;
    FirebaseAuth auth;

    //Drop down List Values
    String[] setListDistrict = new String[]{"Select Category", "Rice ", "Noodles", "Beverage", "Pizza", "Burger", "Kottu", "Other"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(getApplicationContext(), RestaurantSignin.class));
            finish();
        }

        rAddimage = findViewById(R.id.updateImg);
        rplus = findViewById(R.id.Edit_img_plus);

        updateName = findViewById(R.id.updateName);
        updateCategory = findViewById(R.id.updateCategory);
        updateDesc = findViewById(R.id.updateDesc);
        updatePrice = findViewById(R.id.updatePrice);
        updateRadio = findViewById(R.id.updateRadio);
        radio1 = findViewById(R.id.editRadio1);
        radio2 = findViewById(R.id.editRadio2);

        btnUpdate = findViewById(R.id.btnUpdate);

        getUID = getIntent().getExtras().getString("uID");

        Log.d("recievedID", getUID);


        db = FirebaseDatabase.getInstance().getReference().child(Objects.requireNonNull(auth.getCurrentUser()).getUid()).child("Products");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    collectData((Map<String, Object>) Objects.requireNonNull(snapshot.getValue()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        rplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChooseFile(rAddimage);
            }
        });

        // get data to the dropdown
        setDataSpinners();
        //get selected values
        getDataSpinners();

    }

    private void collectData(Map<String, Object> users) {

        int cnt = 0;

        //iterate through each user, ignoring their UID
        for (Map.Entry<String, Object> entry : users.entrySet()) {
            cnt = cnt + 1;
        }
        String[][] arry2 = new String[cnt][7];

        String[] arrTemp = new String[7];

        int tempCount = 0;
        for (Map.Entry<String, Object> entry : users.entrySet()) {

            //Get user map
            Map singleUser = (Map) entry.getValue();

//            arry2[tempCount][0] = (String) singleUser.get("pImage");
//            arry2[tempCount][1] = (String) singleUser.get("pFoodname");
//            arry2[tempCount][2] = (String) singleUser.get("pDescription");
//            arry2[tempCount][3] = (String) singleUser.get("pPrice");
//            arry2[tempCount][4] = (String) singleUser.get("pCetogory");
//            arry2[tempCount][5] = (String) singleUser.get("pDeliveryAvailable");
//            arry2[tempCount][6] = (String) singleUser.get("pID");
            if (String.valueOf(singleUser.get("pID")).equals(getUID)) {
                arrTemp[0] = (String) singleUser.get("pImage");
                arrTemp[1] = (String) singleUser.get("pFoodname");
                arrTemp[2] = (String) singleUser.get("pDescription");
                arrTemp[3] = (String) singleUser.get("pPrice");
                arrTemp[4] = (String) singleUser.get("pCetogory");
                arrTemp[5] = (String) singleUser.get("pDeliveryAvailable");
                arrTemp[6] = (String) singleUser.get("pID");
            }

            tempCount = tempCount + 1;

            String te = String.valueOf(singleUser);
            Log.d("Hell", te);
        }
//        Arrays.sort(arry2, (a, b) -> a[1].compareTo(b[1]));
//        Arrays.sort(arry2, (a, b) -> a[0].compareTo(b[0]));
        loadRecievedData(arrTemp);

    }

    private void getDataSpinners() {

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (updateCategory.getSelectedItem().toString() != "Select a district") {
                    Categ = updateCategory.getSelectedItem().toString();
                }
                foodname = updateName.getText().toString();
                price = updatePrice.getText().toString();
                description = updateDesc.getText().toString();

                updateRadio = findViewById(R.id.Dgroup);
                yradio = ((RadioButton) findViewById(updateRadio.getCheckedRadioButtonId())).getText().toString();


                addimgUri = String.valueOf(rimgUri);

                final String randomKey = UUID.randomUUID().toString();

                ProductModel promodel = new ProductModel(randomKey, addimgUri, foodname, description, price, yradio, Categ);


                db.push().setValue(promodel);
                Toast.makeText(getApplicationContext(), "Data Inserted!", Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void setDataSpinners() {

        //create an adapter to describe how the items are displayed
        ArrayAdapter<String> adapterTemp = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, setListDistrict);

        //set the spinners adapter to the previously created one.
        updateCategory.setAdapter(adapterTemp);

    }

    private void loadRecievedData(String[] arrTemp) {

//        String temString = arrTemp[0];
//        Uri tempUri = Uri.parse(temString);
//        updateImg.setImageURI(tempUri);
        updateName.setText(arrTemp[1]);
        updateDesc.setText(arrTemp[2]);
        updatePrice.setText(arrTemp[3]);
        for(int i = 0;i<setListDistrict.length;i++){

            if(setListDistrict[i].equals(arrTemp[5])){
                updateCategory.setSelection(i);
            }
        }

        if(arrTemp[4].equals("Yes")){
            radio1.setChecked(true);
        }
        else if(arrTemp[4].equals("No")){
            radio2.setChecked(true);
        }


    }

    public void onChooseFile(View v) {
        CropImage.activity().start(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                rimgUri = result.getUri();
                if (setState) {
                    rAddimage.setImageURI(rimgUri);
                } else {
                    rAddimage.setImageResource(R.drawable.cooking);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception e = result.getError();
                Toast.makeText(this, "Possible error is : " + e, Toast.LENGTH_SHORT).show();
            }
        }

    }
}